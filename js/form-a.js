$(function(){
	$('#location-selected label i.fa-times').live('click',function(){
		$(this).parent().fadeOut('100',function(){
			$(this).remove();
		});
	});

	$('select[name=lokasi_kegiatan]').change(function(){
		if($(this).children('option:selected').text() == 'Kecamatan'){
			$('#desa-select').show();
		} else {
			$('#desa-select').hide();
		}
	}).change();

	$(".tambah_multiple").click(function(){
		var tr_clone = $(this).parent().parent().next().clone();
		tr_clone.find("input").each(function(){
			$(this).val("");
		});
		tr_clone.find("textarea").each(function(){
			$(this).val("");
		});
		tr_clone.insertAfter($(this).parent().parent());
		$(this).parent().parent().next().find('.hapus_multiple').show();
	});
	
	$(".hapus_multiple").live('click',function(){
		$(this).parent().parent().parent().remove();
	});

	$('input[name=dim_check]').change(function(){
		if($(this).attr('checked') == 'checked'){
			$('#dim_hidden').show();
		} else {
			$('#dim_hidden').hide();
		}
	});
	$('input[name=dim_check]').change();

});


$(function(){
    $('.kelengkapan_checkbox input:checkbox:checked').parent().next().find('input[type=file]').show();

    $('.kelengkapan_checkbox input:checkbox').click(function(){
        if ($(this).is(':checked')){
            $(this).parents('td').next().find('input[type=file]').show();
        } else {
            $(this).parents('td').next().find('input[type=file]').hide();
        }
    });
});
function selectLocation(el){
	var url = el.prev().children('option:selected').data('url');

	$('#location-modal .modal-content').empty().load(url,function(){
		$('#location-modal').modal('show');
	});
	
}

function confirmSelect(){
	$('.location-list input:checked').each(function(){
		$('#location-selected label').remove();
		$('.location-list input:checked').each(function(){
            var selected = $(this).parent().text();            
            $('#location-selected').show().append('<label>'+selected+'<i class="fa fa-times"></i></label>');
        });
	});
    $('#location-modal').modal('hide');
}

function addDesa(){
	var desa = $('select[name=desa] option:selected').text(),
		kec	 = $('select[name=desa] option:selected').data('kecamatan');

	$('#desa-selected table.data tr.empty').hide();
	$('#desa-selected table.data tbody').append('<tr><td class="action"><a href="#"><i class="fa fa-trash-o"></i></a></td><td>'+kec+'</td><td>'+desa+'</td></tr>');
}