Jogjakartype adalah font yang dikembangkan berbasis dari wordmark logo Jogja Istimewa. Tujuannya agar masyarakat Jogja lebih mudah mengaplikasikan logo tersebut dalam berbagai penggunaan software dan keperluan apapun terutama untuk branding Jogjakarta.

Anda boleh mengunduh, menggunakan dan mendistribusikan Jogjakartype dengan bebas.  Diutamakan untuk keperluan pengenalan dan branding Jogja Istimewa.  Anda dilarang meniru dan atau menjual serta menggunakan untuk keperluan komersial.

Hak cipta logo Jogja melekat pada Pemerintah Daerah Istimewa Jogjakarta.
hak cipta font Jogjakartype pada Locomotype.

--------------------------------------------------------

Arwan Sutanto
Founder Locomotype
http://locomotype.com